# toy-rsa
Toy RSA encryption library implemented in Rust

I implemented the lambda function which calculates the LCM of the values of the parameters subtracted by one. I used all the functions of the toy-rsa-lib library for my task.

I encountered the following error: 'rust attempt to subtract with overflow'. I solved it by using a loop along with break instead of while loop which I was using earlier, which caused me to initialise the key as (0,0) which was causing this issue.

Apart from that I did not encounter any significant issues. I tested the library by using the test case provided in the assignment to set up unit tests, and by implementing an example code that accepts a u32 integer from a user and then encodes and then decodes it using this library, displaying the original message, encrypted message, and the decrypted message in the standard output, where I verified if the original message and the encoded message are the same or not.