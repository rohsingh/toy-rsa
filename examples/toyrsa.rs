/**
 * Code to create an example sourced from: 
 * http://xion.io/post/code/rust-examples.html
 */

use std::io;
use toy_rsa::{decrypt, encrypt, genkey};

extern crate toy_rsa;

fn main() {
    let private_key = genkey();
    let public_key = private_key.0 as u64 * private_key.1 as u64;
    let mut message = 0x12345f;

    /*
     * Code to input integer from standard input sourced from:
     * https://stackoverflow.com/questions/30355185/how-to-read-an-integer-input-from-the-user-in-rust-1-0
     */

    println!("Enter your message (u32 integer): ");
    let mut input_text = String::new();
    io::stdin()
        .read_line(&mut input_text)
        .expect("Failed to read from stdin");

    let trimmed = input_text.trim();
    match trimmed.parse::<u32>() {
        Ok(number) => message = number,
        Err(..) => println!("Not a u32 integer: {}\n\
        Reverting to default value of message: {}\n", trimmed, message),
    };

    let encrypted = encrypt(public_key, message);
    let decrypted = decrypt(private_key, encrypted);

    println!(
        "Message: {}\n\
    Encrypted: {}\n\
    Decrypted: {}\n",
        message, encrypted, decrypted
    );
}
